﻿using Amazon.SimpleNotificationService;
using Amazon.SQS;
using Issue.Localstack.EventBus.MassTransit.SnsSqs.Configuration;
using Issue.Localstack.EventBus.MassTransit.SnsSqs.Models;
using MassTransit;
using MassTransit.AmazonSqsTransport;
using MassTransit.AmazonSqsTransport.Configurators;
using System;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Factories
{
    public static class ConsumerEndpointFactory
    {
        public static ConsumerBusControl Create(SnsSqsConsumerOptions snsSqsConsumerOptions, Action<IAmazonSqsReceiveEndpointConfigurator> endpointConfigurator, byte[] key)
        {
            var busControl = Bus.Factory.CreateUsingAmazonSqs(c =>
            {
                c.UseEncryption(key);
                
                var hostAddress = snsSqsConsumerOptions.HostAddress;
                var hostConfigurator = new AmazonSqsHostConfigurator(hostAddress);
                hostConfigurator.AccessKey(snsSqsConsumerOptions.AccessKey);
                hostConfigurator.SecretKey(snsSqsConsumerOptions.SecretKey);
                if (snsSqsConsumerOptions.HostConfiguration != null)
                {
                    var hostConfiguration = snsSqsConsumerOptions.HostConfiguration;
                    hostConfigurator.Config(new AmazonSimpleNotificationServiceConfig {ServiceURL = hostConfiguration.SnsServiceUrl});
                    hostConfigurator.Config(new AmazonSQSConfig {ServiceURL = hostConfiguration.SqsServiceUrl});
                }
                c.Host(hostConfigurator.Settings);
                
                c.ReceiveEndpoint(snsSqsConsumerOptions.QueueName, endpointConfigurator);
            });

            var consumerBusControl = new ConsumerBusControl(busControl);
            return consumerBusControl;
        }
    }
}