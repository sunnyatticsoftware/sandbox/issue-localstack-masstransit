# issue-localstack-masstransit

## How to reproduce

You need docker and .NET 5 SDK as pre-requirements.

Compile the solution.
```
dotnet build
```

Run a specific version of Localstack
```
docker run -it -e SERVICES=sns,sqs -e TEST_AWS_ACCOUNT_ID="000000000000" -e DEFAULT_REGION="us-east-1" -e LOCALSTACK_HOSTNAME="localhost" -e -rm --privileged --name localstack_main -p 4566:4566 -p 4571:4571 -p 8080-8081:8080-8081  -v "/tmp/localstack:/tmp/localstack" -v "/var/run/docker.sock:/var/run/docker.sock" -e DOCKER_HOST="unix:///var/run/docker.sock" -e HOST_TMP_FOLDER="/tmp/localstack" "localstack/localstack:0.11.2"
```

Run the console app
```
dotnet run --project sample/Issue.Localstack.EventBus.MassTransit.SnsSqs.Sample
```

Notice with the 0.11.2 version works fine, but the version 0.12.8 or 0.11.6 get the topics, subscriptions and queue created but messages are not retrieved and appear as "invisible" in Commandeer.
Don't forget to remove the container and image between tests.

## Stackoverflow question
https://stackoverflow.com/questions/66767011/localstack-with-masstransit-not-getting-messages
