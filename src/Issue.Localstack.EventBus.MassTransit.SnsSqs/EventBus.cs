﻿using MassTransit;
using System.Threading;
using System.Threading.Tasks;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs
{
    public class EventBus
        : IEventBus
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public EventBus(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }
        
        public Task Publish<TEvent>(TEvent @event, CancellationToken cancellationToken) where TEvent : class
        {
            return _publishEndpoint.Publish(@event, cancellationToken);
        }
    }
}