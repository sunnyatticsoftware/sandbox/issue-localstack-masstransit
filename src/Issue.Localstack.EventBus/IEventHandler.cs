﻿using System.Threading.Tasks;

namespace Issue.Localstack.EventBus
{
    public interface IEventHandler
    {
    }
    
    public interface IEventHandler<in TEvent> : IEventHandler
        where TEvent : class
    {
        Task Handle(TEvent @event);
    }
}