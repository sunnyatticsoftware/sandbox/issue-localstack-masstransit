﻿namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Configuration
{
    public class SnsSqsPublisherOptions
        : SnsSqsOptionsBase
    {
        public SnsSqsPublisherOptions(
            string region, 
            string accessKey, 
            string secretKey, 
            HostConfiguration hostConfiguration = null!) 
            : base(region, accessKey, secretKey, hostConfiguration)
        {
        }
    }
}