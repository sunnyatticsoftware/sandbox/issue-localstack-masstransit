﻿using MassTransit;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Models
{
    public class PublisherBusControl
        : BusControl
    {
        public PublisherBusControl(IBusControl busControl)
            : base(busControl)
        {
        }
    }
}