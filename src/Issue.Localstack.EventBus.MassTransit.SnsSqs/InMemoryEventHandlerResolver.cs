﻿using Issue.Localstack.EventBus.MassTransit.SnsSqs.Contracts;
using System;
using System.Collections.Generic;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs
{
    public class InMemoryEventHandlerResolver
        : IEventHandlerResolver
    {
        private readonly IDictionary<Type, object> _registrations = new Dictionary<Type, object>();
        
        public T GetService<T>() 
            where T : class, IEventHandler
        {
            var serviceType = typeof(T);
            var isExisting = _registrations.TryGetValue(serviceType, out var service);
            if (!isExisting)
            {
                throw new KeyNotFoundException($"There is no service registered for {serviceType}");
            }

            return (T)(service ?? throw new InvalidOperationException());
        }

        public InMemoryEventHandlerResolver Add<TService>(TService service)
            where TService : class, IEventHandler
        {
            var serviceType = typeof(TService);
            var isExisting = _registrations.ContainsKey(serviceType);
            if (isExisting)
            {
                throw new ArgumentException($"There is already a service registered for {serviceType}");
            }
            _registrations.Add(serviceType, service);
            return this;
        }
    }
}