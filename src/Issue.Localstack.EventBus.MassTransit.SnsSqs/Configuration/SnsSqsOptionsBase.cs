﻿using System;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Configuration
{
    public abstract class SnsSqsOptionsBase
    {
        public string Region { get; }
        public string AccessKey { get; }
        public string SecretKey { get; }
        public HostConfiguration? HostConfiguration { get; }
        public Uri HostAddress => new Uri($"amazonsqs://{Region}");
        protected SnsSqsOptionsBase(string region, string accessKey, string secretKey, HostConfiguration hostConfiguration)
        {
            Region = region;
            AccessKey = accessKey;
            SecretKey = secretKey;
            HostConfiguration = hostConfiguration;
        }
    }

    public class HostConfiguration
    {
        public string SnsServiceUrl { get; }
        public string SqsServiceUrl { get; }
        public HostConfiguration(string snsServiceUrl, string sqsServiceUrl)
        {
            SnsServiceUrl = snsServiceUrl;
            SqsServiceUrl = sqsServiceUrl;
        }
    }
}