﻿using System;
using System.Threading.Tasks;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Sample
{
    public class FooHandler
        : IEventHandler<Foo>
    {
        public async Task Handle(Foo foo)
        {
            await Console.Out.WriteLineAsync($"Received Foo: {foo}");
        }
    }
    
    public class BarHandler
        : IEventHandler<Bar>
    {
        public async Task Handle(Bar bar)
        {
            await Console.Out.WriteLineAsync($"Received Bar: {bar}");
        }
    }

}