﻿namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Sample
{
    public static class Constants
    {
        public static class SnsSqs
        {
            public const string Region = "localhost:4566";
            public const string AccessKey = "dummy";
            public const string SecretKey = "dummy";
            public const string ServiceUrl = "http://localhost:4566";
        }
    }
}