﻿using System.Threading;
using System.Threading.Tasks;

namespace Issue.Localstack.EventBus
{
    public interface IEventBus
    {
        Task Publish<TEvent>(TEvent @event, CancellationToken cancellationToken = default) where TEvent : class;
    }
}