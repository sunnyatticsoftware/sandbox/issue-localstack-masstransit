﻿namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Sample
{
    public class Foo
    {
        public string Message { get; }

        public Foo(string message)
        {
            Message = message;
        }
        
        public override string ToString()
        {
            return $"{GetType().Name}, Message={Message}";
        }
    }

    public class Bar
    {
        public string Message { get; }
        
        public Bar(string message)
        {
            Message = message;
        }
        
        public override string ToString()
        {
            return $"{GetType().Name}, Message={Message}";
        }
    }
}