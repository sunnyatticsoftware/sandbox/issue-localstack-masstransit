﻿using Issue.Localstack.EventBus.MassTransit.SnsSqs.Contracts;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Factories
{
    public sealed class EventHandlerFactory
    {
        private readonly IEventHandlerResolver _eventHandlerResolver;

        public EventHandlerFactory(IEventHandlerResolver eventHandlerResolver)
        {
            _eventHandlerResolver = eventHandlerResolver;
        }
        
        public IEventHandler<TEvent> Create<TEvent>()
            where TEvent : class
        {
            return _eventHandlerResolver.GetService<IEventHandler<TEvent>>();
        }            
    }
}