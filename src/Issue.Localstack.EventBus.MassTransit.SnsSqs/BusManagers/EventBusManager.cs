﻿using MassTransit;
using MassTransit.Util;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.BusManagers
{
    public class EventBusManager
        : IAsyncDisposable, IDisposable
    {
        private readonly IBusControl _busControl;

        public EventBusManager(IBusControl busControl)
        {
            _busControl = busControl;
        }
        
        public Task Start(CancellationToken cancellationToken = default)
        {
            return _busControl.StartAsync(cancellationToken);
        }

        public Task Stop(CancellationToken cancellationToken = default)
        {
            return _busControl.StopAsync(cancellationToken);
        }
        
        public ValueTask DisposeAsync()
        {
            return new(Stop());
        }

        public void Dispose()
        {
            TaskUtil.Await(() => Stop());
        }
    }
}