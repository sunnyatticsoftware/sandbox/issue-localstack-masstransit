﻿namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Configuration
{
    public class SnsSqsConsumerOptions
        : SnsSqsOptionsBase
    {
        public string QueueName { get; }

        public SnsSqsConsumerOptions(
            string region, 
            string accessKey, 
            string secretKey, 
            string queueName, 
            HostConfiguration hostConfiguration = null!) 
            : base(region, accessKey, secretKey, hostConfiguration)
        {
            QueueName = queueName;
        }
    }
}