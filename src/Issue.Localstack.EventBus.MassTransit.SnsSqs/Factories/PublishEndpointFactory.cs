﻿using Amazon.SimpleNotificationService;
using Amazon.SQS;
using Issue.Localstack.EventBus.MassTransit.SnsSqs.Configuration;
using Issue.Localstack.EventBus.MassTransit.SnsSqs.Models;
using MassTransit;
using MassTransit.AmazonSqsTransport.Configurators;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Factories
{
    public static class PublishEndpointFactory
    {
        public static PublisherBusControl Create(SnsSqsPublisherOptions snsSqsPublisherOptions, byte[] key)
        {
            var busControl = Bus.Factory.CreateUsingAmazonSqs(c =>
            {
                c.UseEncryption(key);
                
                var hostAddress = snsSqsPublisherOptions.HostAddress;
                var hostConfigurator = new AmazonSqsHostConfigurator(hostAddress);
                hostConfigurator.AccessKey(snsSqsPublisherOptions.AccessKey);
                hostConfigurator.SecretKey(snsSqsPublisherOptions.SecretKey);
                if (snsSqsPublisherOptions.HostConfiguration != null)
                {
                    var hostConfiguration = snsSqsPublisherOptions.HostConfiguration;
                    hostConfigurator.Config(new AmazonSimpleNotificationServiceConfig {ServiceURL = hostConfiguration.SnsServiceUrl});
                    hostConfigurator.Config(new AmazonSQSConfig {ServiceURL = hostConfiguration.SqsServiceUrl});
                }
                
                c.Host(hostConfigurator.Settings);
            });
            
            var publisherBusControl = new PublisherBusControl(busControl);
            return publisherBusControl;
        }
    }
}