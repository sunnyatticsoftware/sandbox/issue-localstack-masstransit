﻿using MassTransit;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Models
{
    public abstract class BusControl
    {
        public IBusControl Bus { get; }

        protected BusControl(IBusControl bus)
        {
            Bus = bus;
        }
    }
}