﻿using Issue.Localstack.EventBus.MassTransit.SnsSqs.BusManagers;
using Issue.Localstack.EventBus.MassTransit.SnsSqs.Configuration;
using Issue.Localstack.EventBus.MassTransit.SnsSqs.Factories;
using MassTransit;
using System;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Sample
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            await Console.Out.WriteLineAsync("Sample started..");
         
            var aes = new AesCryptoServiceProvider();
            aes.GenerateKey();
            var key = aes.Key;
            
            var cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
            var cancellationToken = cancellationTokenSource.Token;
            
            await Console.Out.WriteLineAsync("Listening to events..");
            
            var hostConfiguration = 
                new HostConfiguration(Constants.SnsSqs.ServiceUrl, Constants.SnsSqs.ServiceUrl);
            
            var snsSqsConsumerOptions =
                new SnsSqsConsumerOptions(
                    Constants.SnsSqs.Region, 
                    Constants.SnsSqs.AccessKey, 
                    Constants.SnsSqs.SecretKey,
                    "sample-queue",
                    hostConfiguration);

            var consumerBusControl =
                ConsumerEndpointFactory.Create(
                    snsSqsConsumerOptions,
                    endpointConfigurator =>
                    {
                        var dependencyInjectionResolver = new InMemoryEventHandlerResolver();
                        dependencyInjectionResolver.Add<IEventHandler<Foo>>(new FooHandler());
                        dependencyInjectionResolver.Add<IEventHandler<Bar>>(new BarHandler());

                        var eventHandlerFactory = new EventHandlerFactory(dependencyInjectionResolver);

                        var fooHandlerAdapter = new EventHandlerAdapter<Foo>(eventHandlerFactory);
                        var barHandlerAdapter = new EventHandlerAdapter<Bar>(eventHandlerFactory);

                        endpointConfigurator.Consumer(() => fooHandlerAdapter);
                        endpointConfigurator.Consumer(() => barHandlerAdapter);
                    },
                    key);
            var consumerManager = new EventBusManager(consumerBusControl.Bus);
            await consumerManager.Start(cancellationToken);
            
            await Console.Out.WriteLineAsync("Publishing events..");
            
            var snsSqsPublisherOptions = 
                new SnsSqsPublisherOptions(
                    Constants.SnsSqs.Region, 
                    Constants.SnsSqs.AccessKey, 
                    Constants.SnsSqs.SecretKey,
                    hostConfiguration);
            
            var publisherBusControl = PublishEndpointFactory.Create(snsSqsPublisherOptions, key);
            var publisherManager = new EventBusManager(publisherBusControl.Bus);
            await publisherManager.Start(cancellationToken);
            var eventBus = new EventBus(publisherBusControl.Bus);
            await PublishEvents(eventBus, cancellationToken);
            
            Console.WriteLine("Press enter to exit");
            _ = Console.ReadKey();

            await publisherManager.Stop(cancellationToken);
            await consumerManager.Stop(cancellationToken);
        }

        private static async Task PublishEvents(IEventBus eventBus, CancellationToken cancellationToken)
        {
            var foo = new Foo("this is a foo message");
            var bar = new Bar("this is a bar message");
            
            await eventBus.Publish<Foo>(foo, cancellationToken);
            await eventBus.Publish<Bar>(bar, cancellationToken);
        }
    }
}