﻿using MassTransit;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Models
{
    public class ConsumerBusControl
        : BusControl
    {
        public ConsumerBusControl(IBusControl busControl)
            : base(busControl)
        {
        }
    }
}