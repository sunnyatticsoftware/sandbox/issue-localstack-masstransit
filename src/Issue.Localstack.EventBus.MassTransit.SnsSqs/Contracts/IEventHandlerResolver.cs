﻿namespace Issue.Localstack.EventBus.MassTransit.SnsSqs.Contracts
{
    public interface IEventHandlerResolver
    {
        T GetService<T>() where T: class, IEventHandler;
    }
}