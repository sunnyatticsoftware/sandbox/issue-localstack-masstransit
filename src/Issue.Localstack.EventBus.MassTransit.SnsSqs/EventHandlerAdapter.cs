﻿using Issue.Localstack.EventBus.MassTransit.SnsSqs.Factories;
using MassTransit;
using System.Threading.Tasks;

namespace Issue.Localstack.EventBus.MassTransit.SnsSqs
{
    public class EventHandlerAdapter<TEvent>
        : IConsumer<TEvent>
            where TEvent : class
    {
        private readonly EventHandlerFactory _eventHandlerFactory;

        public EventHandlerAdapter(EventHandlerFactory eventHandlerFactory)
        {
            _eventHandlerFactory = eventHandlerFactory;
        }
        
        public Task Consume(ConsumeContext<TEvent> context)
        {
            var handler = _eventHandlerFactory.Create<TEvent>();
            var message = context.Message;
            return handler.Handle(message);
        }
    }
}